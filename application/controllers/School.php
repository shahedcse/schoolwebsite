<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class School extends CI_Controller {

    public function __construct() {
        parent::__construct();
    }

    public function about_us() {
        $data['base_url'] = $this->config->item('base_url');
        $data['page_title'] = "About Us";
        $data['page_voice'] = "About Our Mission Vission And activities";
        $this->load->view('Header', $data);
        $this->load->view('About_us', $data);
        $this->load->view('Footer', $data);
    }

    function chairman_m() {
        $data['base_url'] = $this->config->item('base_url');
        $data['page_title'] = "Chairman Message";
        $data['page_voice'] = "Chairman's Message of Southeast Bank Green School";
        $this->load->view('Header', $data);
        $this->load->view('Chairman', $data);
        $this->load->view('Footer', $data);
    }

    function principle_m() {
        $data['base_url'] = $this->config->item('base_url');
        $data['page_title'] = "Principle Message";
        $data['page_voice'] = "principle's Message of Southeast Bank Green School";
        $this->load->view('Header', $data);
        $this->load->view('Principle', $data);
        $this->load->view('Footer', $data);
    }

    function contact() {
        $data['base_url'] = $this->config->item('base_url');
        $data['page_title'] = "Contact";
        $data['page_voice'] = "Contact Of Southeast Bank Green School";
        $this->load->view('Header', $data);
        $this->load->view('Contact', $data);
        $this->load->view('Footer', $data);
    }

    function staff() {
        $data['base_url'] = $this->config->item('base_url');
        $data['page_title'] = "Staff Development";
        $data['page_voice'] = "Staff of Southeast bank Green School";
        $this->load->view('Header', $data);
        $this->load->view('school_staff', $data);
        $this->load->view('Footer', $data);
    }

    function faq() {
        $data['base_url'] = $this->config->item('base_url');
        $data['page_title'] = "Frequently Asked Questions";
        $data['page_voice'] = "General Question's Answer";
        $this->load->view('Header', $data);
        $this->load->view('faq', $data);
        $this->load->view('Footer', $data);
    }

    function gallery() {
        $data['base_url'] = $this->config->item('base_url');
        $data['page_title'] = "Photo Gallery";
        $data['page_voice'] = "program's photo Gallery of Southeast bank Green School";
        $this->load->view('Header', $data);
        $this->load->view('Photo_gallery', $data);
        $this->load->view('Footer', $data);
    }

}
