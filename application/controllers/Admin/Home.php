<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

    public function __construct() {
        parent::__construct();
    }

    public function index() {
        $data['base_url'] = $this->config->item('base_url');

        $name = $this->input->post('name');
        $pass = $this->input->post('password');

       // if ($name == 'admin' && $pass == 'admin'):
            $data['page_title'] = "Admin Home";

            $this->load->view('Admin/Header', $data);
            $this->load->view('Admin/Sidebar', $data);
            $this->load->view('Admin/Home', $data);
            $this->load->view('Admin/Footer', $data);
      //  else:
         //   redirect($data['base_url']);
      // endif;
    }

    function notice() {
        $data['base_url'] = $this->config->item('base_url');
        $data['page_title'] = "Notice Info";
        $data['active_menu'] = "admin";
        $data['active_sub_menu'] = "notice";

        $this->load->view('Admin/Header', $data);
        $this->load->view('Admin/Sidebar', $data);
        $this->load->view('Admin/Notice', $data);
       // $this->load->view('Admin/Footer', $data);
    }

}
