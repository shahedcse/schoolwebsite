<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

    public function __construct() {
        parent::__construct();
    }

    public function index() {
        $data['base_url'] = $this->config->item('base_url');
        $data['page_title'] = "SB Green School";
        $data['page_voice'] = "Welcome To Southeast Bank Green School";
        $this->load->view('Header', $data);
        $this->load->view('Home', $data);
        $this->load->view('Footer', $data);
    }

}
