<div class="inner-banner">
  <div class="container">
    <div class="col-sm-12">
      <h2>Contact Us</h2>
    </div>
    <div class="col-sm-12 inner-breadcrumb">
      <ul>
          <li><a href="<?php  echo base_url();  ?>">Home</a></li>
        <li>Contact Us</li>
      </ul>
    </div>
  </div>
</div>
<!-- Inner Banner Wrapper End -->
<section class="inner-wrapper contact-wrapper">
  <div class="container">
    <div class="row">
      <div class="inner-wrapper-main">
        <div class="contact-address">
          <div class="col-sm-12 col-md-6 no-space-right">
            <div class="col-sm-6 contact"> <i class="fa fa-map-marker"></i>
              <p><span>Address</span><br>
                House- 15, Road- 12, Mohammadpur, Dhaka.</p>
            </div>
            <div class="col-sm-6 contact white"> <i class="fa fa-phone"></i>
              <p><span>Phone Number</span><br>
                +88-01xxxxxxxxx</p>
            </div>
            <div class="col-sm-6 contact white"> <i class="fa fa-volume-control-phone"></i>
              <p><span>Customer Care</span><br>
                +88-01xxxxxxxxx</p>
            </div>
            <div class="col-sm-6 contact"> <i class="fa fa-envelope"></i>
              <p><span>Email</span><br>
                <a href="mailto:support@yourdomain.com">support@yourdomain.com</a></p>
            </div>
          </div>
          <div class="col-sm-12 col-md-6 no-space-left">
            <div class="form">
              <form action="#" method="post" id="contactFrm" name="contactFrm">
                <input type="text" required placeholder="First Name" value="" name="firstname" class="txt">
                <input type="text" required placeholder="Last Name" value="" name="lastname" class="txt">
                <input type="text" required placeholder="Mobile No" value="" name="mob" class="txt">
                <input type="text" required placeholder="Email" value="" name="email" class="txt">
                <textarea placeholder="Message" name="mess" type="text" class="txt_3"></textarea>
                <input type="submit" value="submit" name="submit" class="txt2">
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div id="google-map">
  <iframe  src="https://maps.google.com/maps?width=100%&amp;height=600&amp;hl=en&amp;q=southeast%20bank%20green%20school%2Cdhaka+(My%20Business%20Name)&amp;ie=UTF8&amp;t=&amp;z=14&amp;iwloc=B&amp;output=embed" frameborder="0" scrolling="no" marginheight="0" marginwidth="0"></iframe>
<!--       <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d198710.35112897935!2d-98.51489117772236!3d38.904562823631146!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x54eab584e432360b%3A0x1c3bb99243deb742!2sUnited+States!5e0!3m2!1sen!2sin!4v1471865832140" allowfullscreen></iframe>-->
        </div>
</section>