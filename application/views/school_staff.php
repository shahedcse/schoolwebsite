<style>
    .box{
        overflow: hidden;
        box-shadow: 0 0 3px rgba(0,0,0,0.3);
        position: relative;
    }
    .box img{
        width: 100%;
        height: auto;
    }
    .box .box-content{
        padding: 25% 10px;
        text-align: center;
        position: absolute;
        top: 0;
        left: 0;
        bottom: 0;
        right: 0;
        opacity: 0;
        z-index: 1;
        transition: all 0.5s ease 0s;
    }
    .box:hover .box-content{
        opacity: 1;
        transition-delay: 0.2s;
    }
    .box:before,
    .box:after,
    .box .box-content:before,
    .box .box-content:after{
        content: "";
        width: 50%;
        height: 50%;
        background: rgba(4, 36, 66, 0.8);
        position: absolute;
        top: 0;
        left: 0;
        opacity: 0;
        transform: scale(0.05);
        transition: all 0.3s ease 0s;
    }
    .box:after{
        left: auto;
        right: 0;
        transition-delay: 0.1s;
    }
    .box .box-content:before,
    .box .box-content:after{
        top: auto;
        bottom: 0;
        z-index: -1;
        transition-delay: 0.2s;
    }
    .box .box-content:after{
        left: auto;
        right: 0;
        transition-delay: 0.3s;
    }
    .box:hover:before,
    .box:hover:after,
    .box:hover .box-content:before,
    .box:hover .box-content:after{
        opacity: 1;
        transform: scale(1);
    }
    .box .title{
        font-size: 20px;
        color: #f5bc24;
        margin: 0 0 10px 0;
    }

    .box .title2{
        font-size: 15px;
        color: #f5bc24;
        margin: 0 0 10px 0;
    }

    .box .post{
        display: block;
        font-size: 14px;
        color: #fff;
        font-weight: 700;
        font-style: italic;
        margin: 0 0 10px 0;
    }
    .box .icon{
        padding: 0;
        margin: 0;
        list-style: none;
    }
    .box .icon li{
        display: inline-block;
        margin: 0 5px 0 0;
    }
    .box .icon li a{
        display: block;
        width: 30px;
        height: 30px;
        line-height: 30px;
        font-size: 15px;
        color: #f5bc24;
        border: 1px solid #f5bc24;
        transition: all 0.3s ease 0s;
    }
    .box .icon li a:hover{
        text-decoration: none;
        border-radius: 50%;
        background: #f5bc24;
        color: #fff;
    }
    @media only screen and (max-width:990px){
        .box{ margin-bottom: 30px; }
    }
</style>
<div class="inner-banner">
    <div class="container">
        <div class="col-sm-12">
            <h2>Southeast Bank Green School</h2>
        </div>
        <div class="col-sm-12 inner-breadcrumb">
            <ul>
                <li><a href="<?php echo base_url(); ?>">Home</a></li>
                <li>Our Staff</li>

            </ul>
        </div>
    </div>
</div>
<!-- Inner Banner Wrapper End -->
<section class="inner-wrapper">
    <div class="container">
        <div class="row">
            <h2>About <span>Our Staff</span></h2>
            <div class="inner-wrapper-main">
                <div class="row">
                    <div class="col-md-3">
                        <div class="box">
                            <img src="<?php echo $base_url; ?>asset/images/team-img1.jpg" alt="">
                            <div class="box-content">
                                <h6 class="title">Mahazabin Islam</h6>
                                <h6 class="title2">csemahazabin@gmail.com</h6>
                                <span class="post">ICT Teacher</span>
                            </div>
                        </div>
                        <p>Nametest</p>
                    </div>
                    <div class="col-md-3">
                        <div class="box">
                            <img src="<?php echo $base_url; ?>asset/images/team-img1.jpg" alt="">
                            <div class="box-content">
                                <h6 class="title">Mahazabin Islam2</h6>
                                <h6 class="title2">csemahazabin@gmail.com</h6>
                                <span class="post">ICT Teacher</span>
                            </div>
                        </div>
                        <p>Nametest</p>
                    </div>
                    <div class="col-md-3">
                        <div class="box">
                            <img src="<?php echo $base_url; ?>asset/images/team-img1.jpg" alt="">
                            <div class="box-content">
                                <h6 class="title">Mahazabin Islam</h6>
                                <h6 class="title2">csemahazabin@gmail.com</h6>
                                <span class="post">ICT Teacher</span>
                            </div>
                        </div>
                        <p>Nametest</p>
                    </div>
                    <div class="col-md-3">
                        <div class="box">
                            <img src="<?php echo $base_url; ?>asset/images/team-img1.jpg" alt="">
                            <div class="box-content">
                                <h6 class="title">Mahazabin Islam</h6>
                                <h6 class="title2">csemahazabin@gmail.com</h6>
                                <span class="post">ICT Teacher</span>
                            </div>
                        </div>
                        <p>Nametest</p>
                    </div>
                    
                </div>
            </div>
        </div>
</section>