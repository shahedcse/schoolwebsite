<div class="inner-banner">
  <div class="container">
    <div class="col-sm-12">
      <h2>Photo Gallery</h2>
    </div>
    <div class="col-sm-12 inner-breadcrumb">
      <ul>
          <li><a href="<?php  echo base_url();  ?>">Home</a></li>
        <li>Gallery</li>
        
      </ul>
    </div>
  </div>
</div>
<!-- Inner Banner Wrapper End -->
<section class="inner-wrapper">
  <div class="container">
    <div class="row">
      <div class="inner-wrapper-main">
        <div class="container gal-container">
          <div class="col-md-4 col-sm-6 co-xs-12 gal-item">
            <div class="box"> <a href="javascript:void(0)" data-toggle="modal" data-target="#1">
              <div class="caption">
                <h4>Gallery Image1</h4>
                <p>Lorem Ipsum is simply dummy text of the printing</p>
                <i class="fa fa-search" aria-hidden="true"></i> </div>
              <img src="<?php  echo base_url();  ?>asset/images/gallery-img1.jpg" alt="Gallery Image"> </a>
              <div class="modal fade" id="1" tabindex="-1" role="dialog">
                <div class="modal-dialog" role="document">
                  <div class="modal-content">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                    <div class="modal-body"> <img src="<?php  echo base_url();  ?>asset/images/gallery-img1.jpg" alt="Gallery Image"> </div>
                    <div class="col-md-12 description">
                      <h4>This is the first one on my Gallery</h4>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="col-md-4 col-sm-6 co-xs-12 gal-item">
            <div class="box"> <a href="javascript:void(0)" data-toggle="modal" data-target="#2">
              <div class="caption">
                <h4>Gallery Image2</h4>
                <p>Lorem Ipsum is simply dummy text of the printing</p>
                <i class="fa fa-search" aria-hidden="true"></i> </div>
              <img src="<?php  echo base_url();  ?>asset/images/gallery-img2.jpg" alt="Gallery Image"> </a>
              <div class="modal fade" id="2" tabindex="-1" role="dialog">
                <div class="modal-dialog" role="document">
                  <div class="modal-content">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                    <div class="modal-body"> <img src="<?php  echo base_url();  ?>asset/images/gallery-img2.jpg" alt="Gallery Image"> </div>
                    <div class="col-md-12 description">
                      <h4>This is the second one on my Gallery</h4>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="col-md-4 col-sm-6 co-xs-12 gal-item">
            <div class="box"> <a href="javascript:void(0)" data-toggle="modal" data-target="#3">
              <div class="caption">
                <h4>Gallery Image3</h4>
                <p>Lorem Ipsum is simply dummy text of the printing</p>
                <i class="fa fa-search" aria-hidden="true"></i> </div>
              <img src="<?php  echo base_url();  ?>asset/images/gallery-img3.jpg" alt="Gallery Image"> </a>
              <div class="modal fade" id="3" tabindex="-1" role="dialog">
                <div class="modal-dialog" role="document">
                  <div class="modal-content">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                    <div class="modal-body"> <img src="<?php  echo base_url();  ?>asset/images/gallery-img3.jpg" alt="Gallery Image"> </div>
                    <div class="col-md-12 description">
                      <h4>This is the third one on my Gallery</h4>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="col-md-4 col-sm-6 co-xs-12 gal-item">
            <div class="box"> <a href="javascript:void(0)" data-toggle="modal" data-target="#4">
              <div class="caption">
                <h4>Gallery Image4</h4>
                <p>Lorem Ipsum is simply dummy text of the printing</p>
                <i class="fa fa-search" aria-hidden="true"></i> </div>
              <img src="<?php  echo base_url();  ?>asset/images/gallery-img4.jpg" alt="Gallery Image"> </a>
              <div class="modal fade" id="4" tabindex="-1" role="dialog">
                <div class="modal-dialog" role="document">
                  <div class="modal-content">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                    <div class="modal-body"> <img src="<?php  echo base_url();  ?>asset/images/gallery-img4.jpg" alt="Gallery Image"> </div>
                    <div class="col-md-12 description">
                      <h4>This is the fourth one on my Gallery</h4>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="col-md-4 col-sm-6 co-xs-12 gal-item">
            <div class="box"> <a href="javascript:void(0)" data-toggle="modal" data-target="#5">
              <div class="caption">
                <h4>Gallery Image5</h4>
                <p>Lorem Ipsum is simply dummy text of the printing</p>
                <i class="fa fa-search" aria-hidden="true"></i> </div>
              <img src="<?php  echo base_url();  ?>asset/images/gallery-img5.jpg" alt="Gallery Image"> </a>
              <div class="modal fade" id="5" tabindex="-1" role="dialog">
                <div class="modal-dialog" role="document">
                  <div class="modal-content">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                    <div class="modal-body"> <img src="<?php  echo base_url();  ?>asset/images/gallery-img5.jpg" alt="Gallery Image"> </div>
                    <div class="col-md-12 description">
                      <h4>This is the fifth one on my Gallery</h4>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="col-md-4 col-sm-6 co-xs-12 gal-item">
            <div class="box"> <a href="javascript:void(0)" data-toggle="modal" data-target="#6">
              <div class="caption">
                <h4>Gallery Image6</h4>
                <p>Lorem Ipsum is simply dummy text of the printing</p>
                <i class="fa fa-search" aria-hidden="true"></i> </div>
              <img src="<?php  echo base_url();  ?>asset/images/gallery-img6.jpg" alt="Gallery Image"> </a>
              <div class="modal fade" id="6" tabindex="-1" role="dialog">
                <div class="modal-dialog" role="document">
                  <div class="modal-content">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                    <div class="modal-body"> <img src="<?php  echo base_url();  ?>asset/images/gallery-img6.jpg" alt="Gallery Image"> </div>
                    <div class="col-md-12 description">
                      <h4>This is the sixth one on my Gallery</h4>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="col-md-4 col-sm-6 co-xs-12 gal-item">
            <div class="box"> <a href="javascript:void(0)" data-toggle="modal" data-target="#7">
              <div class="caption">
                <h4>Gallery Image7</h4>
                <p>Lorem Ipsum is simply dummy text of the printing</p>
                <i class="fa fa-search" aria-hidden="true"></i> </div>
              <img src="<?php  echo base_url();  ?>asset/images/gallery-img7.jpg" alt="Gallery Image"> </a>
              <div class="modal fade" id="7" tabindex="-1" role="dialog">
                <div class="modal-dialog" role="document">
                  <div class="modal-content">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                    <div class="modal-body"> <img src="<?php  echo base_url();  ?>asset/images/gallery-img7.jpg" alt="Gallery Image"> </div>
                    <div class="col-md-12 description">
                      <h4>This is the seventh one on my Gallery</h4>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="col-md-4 col-sm-6 co-xs-12 gal-item">
            <div class="box"> <a href="javascript:void(0)" data-toggle="modal" data-target="#8">
              <div class="caption">
                <h4>Gallery Image8</h4>
                <p>Lorem Ipsum is simply dummy text of the printing</p>
                <i class="fa fa-search" aria-hidden="true"></i> </div>
              <img src="<?php  echo base_url();  ?>asset/images/gallery-img8.jpg" alt="Gallery Image"> </a>
              <div class="modal fade" id="8" tabindex="-1" role="dialog">
                <div class="modal-dialog" role="document">
                  <div class="modal-content">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                    <div class="modal-body"> <img src="<?php  echo base_url();  ?>asset/images/gallery-img8.jpg" alt="Gallery Image"> </div>
                    <div class="col-md-12 description">
                      <h4>This is the eighth one on my Gallery</h4>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="col-md-4 col-sm-6 co-xs-12 gal-item">
            <div class="box"> <a href="javascript:void(0)" data-toggle="modal" data-target="#9">
              <div class="caption">
                <h4>Gallery Image9</h4>
                <p>Lorem Ipsum is simply dummy text of the printing</p>
                <i class="fa fa-search" aria-hidden="true"></i> </div>
              <img src="<?php  echo base_url();  ?>asset/images/gallery-img9.jpg" alt="Gallery Image"> </a>
              <div class="modal fade" id="9" tabindex="-1" role="dialog">
                <div class="modal-dialog" role="document">
                  <div class="modal-content">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                    <div class="modal-body"> <img src="<?php  echo base_url();  ?>asset/images/gallery-img9.jpg" alt="Gallery Image"> </div>
                    <div class="col-md-12 description">
                      <h4>This is the nineth one on my Gallery</h4>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
