<div class="inner-banner">
  <div class="container">
    <div class="col-sm-12">
      <h2>Southeast Bank Green School</h2>
    </div>
    <div class="col-sm-12 inner-breadcrumb">
      <ul>
          <li><a href="<?php  echo base_url();  ?>">Home</a></li>
         <li>FAQ</li>
      </ul>
    </div>
  </div>
</div>
<!-- Inner Banner Wrapper End -->
<section class="inner-wrapper">
  <div class="container">
    <div class="row">
      <div class="inner-wrapper-main">
        <h2>Frequently Asked <span>Questions</span></h2>
        <div class="col-sm-8">
          <div id="accordion-first" class="clearfix">
            <div class="accordion" id="accordion1">
              <div class="accordion-group ">
                <div class="accordion-heading"> <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion1" href="#collapseOne"> <em class="icon-fixed-width fa fa-plus"></em>Lorem Ipsum is fully responsive and perfectly fits? </a> </div>
                <div style="height: 0px;" id="collapseOne" class="accordion-body collapse ">
                  <div class="accordion-inner"> Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor... </div>
                </div>
              </div>
              <div class="accordion-group">
                <div class="accordion-heading"> <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion1" href="#collapseTwo"> <em class="icon-fixed-width fa fa-plus"></em>Lorem Ipsum is fully responsive and perfectly fits? </a> </div>
                <div style="height: 0px;" id="collapseTwo" class="accordion-body collapse">
                  <div class="accordion-inner"> Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor... </div>
                </div>
              </div>
              <div class="accordion-group">
                <div class="accordion-heading"> <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion1" href="#collapseThree"> <em class="icon-fixed-width fa fa-plus"></em>Lorem Ipsum is fully responsive and perfectly fits? </a> </div>
                <div style="height: 0px;" id="collapseThree" class="accordion-body collapse">
                  <div class="accordion-inner"> Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor... </div>
                </div>
              </div>
              <div class="accordion-group">
                <div class="accordion-heading"> <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion1" href="#collapseFour"> <em class="icon-fixed-width fa fa-plus"></em>Lorem Ipsum is Powerful Admin Panel? </a> </div>
                <div style="height: 0px;" id="collapseFour" class="accordion-body collapse">
                  <div class="accordion-inner"> Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor... </div>
                </div>
              </div>
              <div class="accordion-group">
                <div class="accordion-heading"> <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion1" href="#collapseFive"> <em class="icon-fixed-width fa fa-plus"></em>Lorem Ipsum is fully responsive and perfectly fits? </a> </div>
                <div style="height: 0px;" id="collapseFive" class="accordion-body collapse">
                  <div class="accordion-inner"> Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor... </div>
                </div>
              </div>
            </div>
            <!-- end accordion -->
          </div>
        </div>
        <div class="col-sm-4">
          <div class="download-services">
            <h3>Please Download our latest Prospectus </h3>
            <a href="#">Download</a> </div>
        </div>
      </div>
    </div>
  </div>
</section>

