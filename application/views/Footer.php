<!-- Footer Links Start-->
<html lang="en">  
    <style>
        #myBtn {
            display: none;
            position: fixed;
            bottom: 20px;
            right: 30px;
            z-index: 99;
            font-size: 18px;
            border: none;
            outline: none;
            background-color: red;
            color: white;
            cursor: pointer;
            padding: 15px;
            border-radius: 4px;
        }

        #myBtn:hover {
            background-color: #555;
        }
    </style>
    <body>
        <footer>
            <div class="container">
                <div class="col-sm-3"><img src="<?php echo $base_url; ?>asset/images/logo.jpg" alt="World Education"> </div>
                <div class="col-sm-5">
                    <div class="contactus">
                        <h2>Contact Us</h2>
                        <ul class="list-ul">
                            <li><i class="fa fa-map-marker"></i>House- 15, Road- 12, Mohammadpur, Dhaka.</li>
                            <li><i class="fa fa-phone"></i>+88-01xxxxxxxx</li>
                            <li><i class="fa fa-envelope"></i><a href="mailto:support@yourdomain.com">support@yourdomain.com</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-sm-4 subscirbe pull-right">
                    <h2>Newsletter</h2>
                    <p class="sub"><span>Subscribe</span> to Our Newsletter to get Important Blog Posts &amp; Inside Scoops:</p>
                    <div class="form">
                        <input type="text" placeholder="Enter your Email" id="exampleInputName" required="" class="form-control first">
                        <input type="button" class="bttn" onclick="checkemail();" value="Subscribe">
                    </div>
                </div>
            </div>
        </footer>

        <!-- Footer Links End -->
        <!-- Copy Rights Start -->
        <div class="footer-wrapper">
            <div class="container">
                <p>&copy; Copyright 
                    <script type="text/javascript">
                        var d = new Date();
                        document.write(d.getFullYear());
                    </script> 
                    Southeast Bank Green School | All Rights Reserved.</p>
            </div>
            <button onclick="topFunction()" id="myBtn" title="Go to top"><i class="fa fa-long-arrow-up" aria-hidden="true"></i></button>

        </div>
    </body>
</html>
<!-- Copy Rights End --> 
<!-- jQuery (necessary for Bootstrap's JavaScript plugins) --> 
<script src="<?php echo $base_url; ?>asset/assets/jquery/jquery-3.1.1.min.js"></script> 
<script src="<?php echo $base_url; ?>asset/assets/jquery/jquery.animateNumber.min.js"></script> 
<!-- Include all compiled plugins (below), or include individual files as needed --> 
<script src="<?php echo $base_url; ?>asset/assets/easing/jquery.easing.min.js"></script> 
<script src="<?php echo $base_url; ?>asset/assets/bootstrap/js/bootstrap.min.js"></script> 
<script src="<?php echo $base_url; ?>asset/assets/wow/wow.min.js"></script> 
<script src="<?php echo $base_url; ?>asset/assets/owl-carousel/js/owl.carousel.js"></script> 
<script src="<?php echo $base_url; ?>asset/js/custom.js"></script>

<script class="include" type="text/javascript" src="<?php echo $base_url; ?>asset/js/jquery.dcjqaccordion.2.7.js"></script>
    <script src="<?php echo $base_url; ?>asset/js/jquery.scrollTo.min.js"></script>
    <script src="<?php echo $base_url; ?>asset/js/jquery.nicescroll.js" type="text/javascript"></script>
    <script type="text/javascript" language="javascript" src="<?php echo $base_url; ?>asset/assets/advanced-datatable/media/js/jquery.dataTables.js"></script>
    <script type="text/javascript" src="<?php echo $base_url; ?>asset/assets/data-tables/DT_bootstrap.js"></script>
     <script src="<?php echo $base_url; ?>asset/js/respond.min.js" ></script>

    <!--right slidebar-->
    <script src="<?php echo $base_url; ?>asset/js/slidebars.min.js"></script>

    <!--dynamic table initialization -->
    <script src="<?php echo $base_url; ?>asset/js/dynamic_table_init.js"></script>


    <!--common script for all pages-->
    <script src="<?php echo $base_url; ?>asset/js/common-scripts.js"></script>
<!-- Global Site Tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-83282272-3"></script>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
<script type="text/javascript" src="<?php echo $base_url; ?>asset/news/js/marquee.js"></script>
<script src='https://code.responsivevoice.org/responsivevoice.js'></script>


<script>

                $(document).ready(function() {
                    $.getScript(responsiveVoice.speak("<?php echo $page_voice; ?>"));
                });

                function checkword() {
                    var word = $("#word").val();
                    if (word == '') {
                        alert('Plaese Enter word Properly');
                    }
                    if (word != '') {
                        $.getScript(responsiveVoice.speak(word));
                       // $("#word").val("");
                    }
                }
</script>

<script>
// When the user scrolls down 20px from the top of the document, show the button
    window.onscroll = function() {
        scrollFunction()
    };

    function scrollFunction() {
        if (document.body.scrollTop > 20 || document.documentElement.scrollTop > 20) {
            document.getElementById("myBtn").style.display = "block";
        } else {
            document.getElementById("myBtn").style.display = "none";
        }
    }

// When the user clicks on the button, scroll to the top of the document
    function topFunction() {
        document.body.scrollTop = 0;
        document.documentElement.scrollTop = 0;
    }
</script>
<script>
    function checkemail() {
        var email = $("#exampleInputName").val();
        if (email == '') {
            alert('Please insert your Email Address');
        }
        else {
            $("#exampleInputName").val();
            alert('Thanks For your Subscription');
        }
    }

    $(function() {
        $('.simple-marquee-container').SimpleMarquee();
    });

</script>
<script>
    window.dataLayer = window.dataLayer || [];
    function gtag() {
        dataLayer.push(arguments)
    }
    ;
    gtag('js', new Date());

    gtag('config', 'UA-83282272-3');
</script>