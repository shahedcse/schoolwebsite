<aside>
    <div id="sidebar"  class="nav-collapse ">
        <!-- sidebar menu start-->
        <ul class="sidebar-menu" id="nav-accordion">
            <li>
                <a class="" href="">
                    <i class="fa fa-dashboard"></i>
                    <span>Dashboard</span>
                </a>
            </li>

            <li class="sub-menu">
                <a href="javascript:;" class="<?php echo (isset($active_menu) && ($active_menu == 'admin')) ? 'active' : ''; ?>" >
                    <i class="fa fa-laptop"></i>
                    <span>Admin</span>
                </a>
                <ul class="sub">
                    <li class="<?php echo (isset($active_sub_menu) && ($active_sub_menu == 'notice')) ? 'active' : ''; ?>"><a  href="<?php echo site_url('Admin/Home/notice'); ?>">Notice</a></li>
               </ul>
            </li>
        </ul>
    </div>
</aside>



