<!DOCTYPE html>
<html lang="en">
    <body>
        <div id="dvLoading"></div>
        <div class="banner-wrapper">
            <div class="carousel fade-carousel slide" data-ride="carousel" data-interval="4000" id="bs-carousel">
                <!-- Overlay -->
                <div class="overlay"></div>
                <!-- Indicators -->
                <ol class="carousel-indicators">
                    <li data-target="#bs-carousel" data-slide-to="0" class="active"></li>
                    <li data-target="#bs-carousel" data-slide-to="1"></li>
                    <li data-target="#bs-carousel" data-slide-to="2"></li>
                </ol>
                <!-- Wrapper for slides -->
                <div class="carousel-inner">
                    <div class="item slides active">
                        <div class="slide-1"></div>
                        <div class="hero">
                            <h1 class="animated1">Education World</h1>
                            <h3 class="animated2">Duis aute irure dolor in reprehenderit in voluptate velit esse</h3>
                        </div>
                    </div>
                    <div class="item slides">
                        <div class="slide-2"></div>
                        <div class="hero">
                            <h1 class="animated1">Education World</h1>
                            <h3 class="animated2">Duis aute irure dolor in reprehenderit in voluptate velit esse</h3>
                        </div>
                    </div>
                    <div class="item slides">
                        <div class="slide-3"></div>
                        <div class="hero">
                            <h1 class="animated1">Education World</h1>
                            <h3 class="animated2">Duis aute irure dolor in reprehenderit in voluptate velit esse</h3>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Banner Wrapper End -->
        <!-- latest news start -->
        <div class="content">
            <div class="simple-marquee-container">
                <div class="marquee-sibling">
                    <strong>Latest Notice:</strong>
                </div>
                <div class="marquee">
                    <ul class="marquee-content-items">
                        <li>Item 1</li>
                        <li>Item 2</li>
                        <li>Item 3</li>
                        <li>Item 4</li>
                        <li>Item 5</li>
                    </ul>
                </div>
            </div>
        </div>
        <!-- latest news End-->
        <!-- About Us -->
        <div class="about-us">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12 col-md-8">
                        <h2>Few Words <br>
                            About <span>Southeast Bank Green School.</span></h2>
                        <p><strong>Lorem ipsum</strong> dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. </p>
                        <p>Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
                        <a href="<?php echo base_url(); ?>School/about_us">More...</a> </div>
                    <div class="col-sm-12 col-md-4 pull-right hidden-sm"> <img src="<?php echo $base_url; ?>asset/images/about-img.jpg" alt="World-edu"> </div>
                </div>
            </div>
        </div>
        <!-- Callouts Wrapper Start -->
        <div class="callouts-wrapper">
            <div class="container">
                <h2>Welcome to <span>Education World</span></h2>
                <p class="center">Lorem Ipsum is simply dummy text of the printing and typesetting industry. printing and typesetting industry.</p>
                <p class="center">Lorem Ipsum has been the industry's standard dummy text ever</p>
                <div class="row">
                    <div class="col-sm-6 col-md-4 wow fadeIn animated" data-wow-duration="1.5s">
                        <div class="callouts">
                            <div class="icon"><i class="fa fa-desktop" aria-hidden="true"></i></div>
                            <div class="content">
                                <h3>Fully Responsive</h3>
                                <p>Coccaecat cupidatat aliqu proident sunt.</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6 col-md-4 wow fadeIn animated" data-wow-duration="1.5s" data-wow-delay="0.3s">
                        <div class="callouts">
                            <div class="icon"> <i class="fa fa-paint-brush" aria-hidden="true"></i></div>
                            <div class="content">
                                <h3>Clean Design</h3>
                                <p>Coccaecat cupidatat aliqu proident sunt.</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6 col-md-4 wow fadeIn animated" data-wow-duration="1.5s" data-wow-delay="0.5s">
                        <div class="callouts">
                            <div class="icon"><i class="fa fa-magic" aria-hidden="true"></i></div>
                            <div class="content">
                                <h3>Retina Ready</h3>
                                <p>Coccaecat cupidatat aliqu proident sunt.</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6 col-md-4 wow fadeIn animated" data-wow-duration="1.5s">
                        <div class="callouts">
                            <div class="icon"><i class="fa fa-cogs" aria-hidden="true"></i></div>
                            <div class="content">
                                <h3>Multipurpose</h3>
                                <p>Coccaecat cupidatat aliqu proident sunt.</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6 col-md-4 wow fadeIn animated" data-wow-duration="1.5s" data-wow-delay="0.3s">
                        <div class="callouts">
                            <div class="icon"> <i class="fa fa-users" aria-hidden="true"></i></div>
                            <div class="content">
                                <h3>Customer Support</h3>
                                <p>Coccaecat cupidatat aliqu proident sunt.</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6 col-md-4 wow fadeIn animated" data-wow-duration="1.5s" data-wow-delay="0.5s">
                        <div class="callouts">
                            <div class="icon"><i class="fa fa-envelope-o" aria-hidden="true"></i></div>
                            <div class="content">
                                <h3>Marketing</h3>
                                <p>Coccaecat cupidatat aliqu proident sunt.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Callouts Wrapper End -->
        <!-- Satisfied Wrapper start -->
        <div class="satisfied-wrapper">
            <div class="container">
                <h2>Statistics of <span>Education</span></h2>
                <p class="center">Lorem Ipsum is simply dummy text of the printing and typesetting industry. printing and typesetting industry.</p>
                <p class="center">Lorem Ipsum has been the industry's standard dummy text ever</p>
                <div class="statistics">
                    <div class="col-sm-3 counter"> <i class="fa fa-list-alt" aria-hidden="true"></i>
                        <div class="number animateNumber" data-num="28"> <span>28</span></div>
                        <p>Our Branches</p>
                    </div>
                    <div class="col-sm-3 counter"> <i class="fa fa-user" aria-hidden="true"></i>
                        <div class="number animateNumber" data-num="180"> <span>180</span></div>
                        <p>Our staff</p>
                    </div>
                    <div class="col-sm-3 counter"> <i class="fa fa-users" aria-hidden="true"></i>
                        <div class="number animateNumber" data-num="3600"> <span>3600</span></div>
                        <p>Students</p>
                    </div>
                    <div class="col-sm-3 counter"> <i class="fa fa-graduation-cap" aria-hidden="true"></i>
                        <div class="number animateNumber" data-num="768"> <span>768</span></div>
                        <p>Graduates</p>
                    </div>
                </div>
            </div>
        </div>

        <div class="team-wrapper">
            <div class="container">
                <div class="row">
                    <h2>Meet The <span>Leadership Team</span></h2>
                    <div id="owl-demo" class="owl-carousel owl-theme">
                        <div class="item">
                            <div class="img-box"> <img src="<?php echo $base_url; ?>asset/images/team-img1.jpg" alt="Team1" title="Team1" />
                                <div class="text-center">
                                    <h4>Sarah Norris</h4>
                                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
                                </div>
                            </div>
                        </div>
                        <div class="item">
                            <div class="img-box"> <img src="<?php echo $base_url; ?>asset/images/team-img2.jpg" alt="Team2" title="Team2" />
                                <div class="text-center">
                                    <h4>Doris Wilson</h4>
                                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
                                </div>
                            </div>
                        </div>
                        <div class="item">
                            <div class="img-box"> <img src="<?php echo $base_url; ?>asset/images/team-img3.jpg" alt="Team3" title="Team3" />
                                <div class="text-center">
                                    <h4>Anne Kemper</h4>
                                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
                                </div>
                            </div>
                        </div>
                        <div class="item">
                            <div class="img-box"> <img src="<?php echo $base_url; ?>asset/images/team-img4.jpg" alt="Team4" title="Team4" />
                                <div class="text-center">
                                    <h4>Ruth Carman</h4>
                                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
                                </div>
                            </div>
                        </div>
                        <div class="item">
                            <div class="img-box"> <img src="<?php echo $base_url; ?>asset/images/team-img1.jpg" alt="Team1" title="Team1" />
                                <div class="text-center">
                                    <h4>Sarah Norris</h4>
                                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
                                </div>
                            </div>
                        </div>
                        <div class="item">
                            <div class="img-box"> <img src="<?php echo $base_url; ?>asset/images/team-img2.jpg" alt="Team2" title="Team2" />
                                <div class="text-center">
                                    <h4>Doris Wilson</h4>
                                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
                                </div>
                            </div>
                        </div>
                        <div class="item">
                            <div class="img-box"> <img src="<?php echo $base_url; ?>asset/images/team-img3.jpg" alt="Team3" title="Team3" />
                                <div class="text-center">
                                    <h4>Anne Kemper</h4>
                                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
                                </div>
                            </div>
                        </div>
                        <div class="item">
                            <div class="img-box"> <img src="<?php echo $base_url; ?>asset/images/team-img4.jpg" alt="Team4" title="Team4" />
                                <div class="text-center">
                                    <h4>Ruth Carman</h4>
                                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Faculty Wrapper End -->
        <!-- Testimonials Wrapper Start -->
        <div class="testimonials-wrapper">
            <div class="container">
                <h2>Our <span>Testimonials</span></h2>
                <div id="testimonials" class="owl-carousel owl-theme">
                    <div class="item"> <img src="<?php echo $base_url; ?>asset/images/testimonials1.png" alt="Testimonials">
                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
                        <h3>- Michal Marek <span>General manager</span></h3>
                    </div>
                    <div class="item"> <img src="<?php echo $base_url; ?>asset/images/testimonials2.png" alt="Testimonials">
                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
                        <h3>- Mary Williams <span>Service manager</span></h3>
                    </div>
                    <div class="item"> <img src="<?php echo $base_url; ?>asset/images/testimonials3.png" alt="Testimonials">
                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
                        <h3>- Michal Marek <span>General manager</span></h3>
                    </div>
                    <div class="item"> <img src="<?php echo $base_url; ?>asset/images/testimonials1.png" alt="Testimonials">
                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
                        <h3>- Michal Marek <span>General manager</span></h3>
                    </div>
                    <div class="item"> <img src="<?php echo $base_url; ?>asset/images/testimonials2.png" alt="Testimonials">
                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
                        <h3>- Mary Williams <span>Service manager</span></h3>
                    </div>
                    <div class="item"> <img src="<?php echo $base_url; ?>asset/images/testimonials3.png" alt="Testimonials">
                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
                        <h3>- Michal Marek <span>General manager</span></h3>
                    </div>
                </div>
            </div>
        </div>
        <!-- Testimonials Wrapper End -->
        <!-- sponsers Start -->
        <div class="sponsers">
            <div class="container">
                <h2>Our <span>Sponsers</span></h2>
                <div id="sponsers" class="owl-carousel owl-theme">
                    <div class="item"> <img src="<?php echo $base_url; ?>asset/images/edu-logo1.jpg" alt="Education Logos"></div>
                    <div class="item"> <img src="<?php echo $base_url; ?>asset/images/edu-logo2.jpg" alt="Education Logos"></div>
                    <div class="item"> <img src="<?php echo $base_url; ?>asset/images/edu-logo3.jpg" alt="Education Logos"></div>
                    <div class="item"> <img src="<?php echo $base_url; ?>asset/images/edu-logo4.jpg" alt="Education Logos"></div>
                    <div class="item"> <img src="<?php echo $base_url; ?>asset/images/edu-logo5.jpg" alt="Education Logos"></div>
                    <div class="item"> <img src="<?php echo $base_url; ?>asset/images/edu-logo6.jpg" alt="Education Logos"></div>
                    <div class="item"> <img src="<?php echo $base_url; ?>asset/images/edu-logo1.jpg" alt="Education Logos"></div>
                    <div class="item"> <img src="<?php echo $base_url; ?>asset/images/edu-logo2.jpg" alt="Education Logos"></div>
                    <div class="item"> <img src="<?php echo $base_url; ?>asset/images/edu-logo3.jpg" alt="Education Logos"></div>
                </div>
            </div>
        </div>
    </body>
</html>

