<style>
    img {
        border-radius: 50%;
    }
</style>
<div class="inner-banner">
    <div class="container">
        <div class="col-sm-12">
            <h2>Southeast Bank Green School</h2>
        </div>
        <div class="col-sm-12 inner-breadcrumb">
            <ul>
                <li><a href="<?php echo base_url(); ?>">Home</a></li>
                <li>Principle Message</li>

            </ul>
        </div>
    </div>
</div>
<section class="inner-wrapper">
    <div class="container">
        <div class="row">
            <h2>Principle Message</h2>
            <div class="inner-wrapper-main">
                <div class="col-sm-12">
                    <div class="light-bg space-30 inset-left-30 inset-right-30">
                        <center> 
                            <img src="<?php echo $base_url; ?>asset/images/demo.png" alt="Avatar" style="width:200px">
                            <h3>Name:Md Test</h3>
                        </center>
                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic. Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
