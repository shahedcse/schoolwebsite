<div class="inner-banner">
    <div class="container">
        <div class="col-sm-12">
            <h2>Southeast Bank Green School</h2>
        </div>
        <div class="col-sm-12 inner-breadcrumb">
            <ul>
                <li><a href="<?php echo base_url(); ?>">Home</a></li>
                <li>About Us</li>

            </ul>
        </div>
    </div>
</div>
<section class="inner-wrapper">
    <div class="row offset-top-5">
        <h2>About <span>Us</span></h2>
        <div class="inner-wrapper-main">
            <div class="col-sm-8 col-md-8 col-sm-offset-2 col-xs-offset-0">
                <div class="button-vertical-tab-panel">
                    <div class="col-sm-4 col-md-3">
                        <nav class="nav-sidebar">
                            <ul class="nav tabs">
                                <li class="active"><a href="#button-tab1" data-toggle="tab">About Us</a></li>
                                <li class=""><a href="#button-tab2" data-toggle="tab">Our Mission</a></li>
                                <li class=""><a href="#button-tab3" data-toggle="tab">Academic Expectations</a></li>
                                <li class=""><a href="#button-tab4" data-toggle="tab">Civic And Social Expectations</a></li>
                                <li class=""><a href="#button-tab5" data-toggle="tab">Other Activities</a></li>
                            </ul>
                        </nav>
                    </div>
                    <div class="tab-content col-sm-8 col-md-9">
                        <div class="tab-pane active text-style" id="button-tab1">
                            <strong>About Us:</strong><br>
                            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.  when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p            >
                        </div>
                        <div class="tab-pane  text-style" id="button-tab2">
                            <strong>Our Mission:</strong><br>
                            <p>The singularity of our mission is embodied in programs that are qualitatively and quantitatively distinctive in their depth, breadth, methodology and excellence. Today’s new age learners have superior intellectual, social, and emotional needs. They need an open, enriched academic periphery designed to foster superior cognitive skills. Southeast Bank Green School provides homely, congenial, positive, enthusiastic school atmosphere that upholds and nurtures an effective educational scenario.</p>
                        </div>
                        <div class="tab-pane text-style" id="button-tab3">
                            <strong>Academic Expectation:</strong><br>
                            <p>
                                • Communicate clearly and effectively through reading, writing. <br>
                                • Demonstrate creative, critical and independent thinking. <br>
                                • Demonstrate self-awareness of healthy behaviors and lifelong wellness. <br>
                                • Observe situations objectively to clearly and accurately define and solve problems. <br>
                                • Use technology effectively. <br>
                                • Have effective work place skills.
                            </p>
                        </div>
                        <div class="tab-pane text-style" id="button-tab4">
                            <strong>Civic and social expectation</strong><br>
                            <p>
                                • Be respectful, responsible and informed. <br>
                                • Be active participants in their own learning. <br>
                                • Consistently demonstrate responsible and ethical behaviors attitudes and choices . <br>
                            </p>
                        </div>
                        <div class="tab-pane text-style" id="button-tab5">
                            <strong>Other Activities</strong><br>
                            <p>
                                1. Final-Term has been being started from January, 2017 for the session 2016-2017.<br>
                                2. Following extracurricular activities events has been taken place during this session until now:<br>
                                &nbsp;&nbsp; &nbsp; a. Annual Sports.<br>
                                &nbsp;&nbsp; &nbsp; b. Celebration of Bangla Literature Day.<br>
                                &nbsp;&nbsp; &nbsp; c. Observation of International Mother Language Day.<br>
                                &nbsp;&nbsp; &nbsp; d. Story Telling Competition.<br>
                                &nbsp;&nbsp; &nbsp; e. Math Visit (Mathematics competition).<br>
                                3. Within this month, we are also going to celebrate ‘National Children’s Day and Birthday of the father of the nation’ and ‘Independence Day’.
                            </p>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
