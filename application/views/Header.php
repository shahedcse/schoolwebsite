<html lang="en">   
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <title><?php echo $page_title; ?></title>
        <!-- Bootstrap CSS -->
        <link href="<?php echo $base_url; ?>asset/assets/bootstrap/css/bootstrap.min.css" rel="stylesheet">
        <!-- Font Awesome CSS-->
        <link href="<?php echo $base_url; ?>asset/assets/font-awesome/css/font-awesome.min.css" rel="stylesheet">
        <!-- Custom CSS -->
        <link href="<?php echo $base_url; ?>asset/css/style.css" rel="stylesheet">
        <!-- Animate CSS -->
        <link href="<?php echo $base_url; ?>asset/assets/animate/animate.css" rel="stylesheet">
        <!-- Owl Carousel -->
        <link href="<?php echo $base_url; ?>asset/assets/owl-carousel/css/owl.carousel.css" rel="stylesheet">
        <link href="<?php echo $base_url; ?>asset/assets/owl-carousel/css/owl.theme.css" rel="stylesheet">
        <!-- Favicon -->
        <link rel="shortcut icon" type="image/x-icon" href="<?php echo $base_url; ?>asset/images/logo.jpg">
        <link rel="stylesheet" href="<?php echo $base_url; ?>asset/news/css/marquee.css" />
        <link rel="stylesheet" href="<?php echo $base_url; ?>asset/news/css/example.css" />

        <link href="<?php echo $base_url; ?>asset/assets/advanced-datatable/media/css/demo_page.css" rel="stylesheet" />
        <link href="<?php echo $base_url; ?>asset/assets/advanced-datatable/media/css/demo_table.css" rel="stylesheet" />
        <link rel="stylesheet" href="<?php echo $base_url; ?>asset/assets/data-tables/DT_bootstrap.css" />
        <!--right slidebar-->
        <link href="<?php echo $base_url; ?>asset/css/slidebars.css" rel="stylesheet">
        <!-- Custom styles for this template -->
 <link href="<?php echo $base_url; ?>asset/css/style-responsive.css" rel="stylesheet" />

        <style>
            #word {
                padding-right: 17px;
                height:25px;
                background-color:blue;
                color:white;
            }
        </style>
    </head>
    <header>
        <div class="top-wrapper hidden-xs">
            <div class="container">
                <div class="col-md-8 col-sm-6 hidden-xs top-wraper-left no-padding">
                    <ul class="header-social-icons">
                        <li class="facebook"><a href="javascript:void(0)" target="_blank"><i class="fa fa-facebook"></i></a></li>
                        <li class="twitter"><a href="javascript:void(0)" target="_blank"><i class="fa fa-twitter"></i></a></li>
                        <li class="linkedin"><a href="javascript:void(0)" target="_blank"><i class="fa fa-linkedin"></i></a></li>||
                        <li class="dribbble"></li>
                        <li class="dribbble"></li>
                        <li class="dribbble"><b>Speeling Test:</b></li>
                        <li>
                            <input placeholder="Enter Text" type="text" id="word" name="word" >
                        </li>
                        <li ><a onclick="checkword()"><img style="height: 30px;" src='<?php echo $base_url; ?>asset/images/mic.png'></a></li>
                    </ul>
                </div>
                <div class="col-md-4 col-sm-6">
                    <ul class="top-right pull-right ">
                        <!-- Login -->
                        <li class="login">   <?php echo date('l,F j, Y'); ?></li>
                        <li class="login"><a href="javascript:void(0)"><i class="fa fa-lock"></i>Login</a>
                            <div class="login-form">
                                <h4>Login</h4>
                                <form action="<?php base_url(); ?>Admin/Home" method="post">
                                    <input type="text" name="name" placeholder="Username">
                                    <input type="password" name="password" placeholder="Password">
                                    <button type="submit" class="btn">Login</button>
                                </form>
                            </div>
                        </li>
                        <!-- Register -->
                        <li class="search"><i class="fa fa-search" aria-hidden="true"></i>
                            <div id="search-form-container">
                                <form id="search-form" action="#" style="display: none;" class="">
                                    <input type="search" id="search" name="search" placeholder="Search...">
                                    <input type="submit" id="search-submit" value="">
                                    <span id="close-form" class="close">x</span>
                                </form>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </div>

        <div class="logo-bar hidden-xs">
            <div class="container">
                <div class="row">
                    <div class="col-sm-2"><a href="<?php echo base_url(); ?>"> <img src="<?php echo $base_url; ?>asset/images/logo.jpg" style="height: 70px;" alt="Education World"></a> </div>
                    <div class="col-sm-5">
                        <h3 class="my-4" style="color:green; font:bold;">
                            <marquee behavior="scroll" direction="Left">Welcome To Southeast Bank Green School. </marquee>
                        </h3>
                    </div>
                    <div class="col-sm-5">
                        <ul class="contact-info pull-right">
                            <li><i class="fa fa-phone"></i>
                                <p> <span>Call us</span><br>
                                    +88-01xxxxxxxxx</p>
                            </li>
                            <li><i class="fa fa-envelope"></i>
                                <p><span>Email Us</span><br>
                                    <a href="mailto:support@sbtechnosoft.com">support@sbtechnosoft.com</a></p>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="wow fadeInDown navigation" data-offset-top="197" data-spy="affix">
            <div class="container">
                <nav class="navbar navbar-default">
                    <div class="row">
                        <!-- Brand and toggle get grouped for better mobile display -->
                        <div class="navbar-header">
                            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false"> <span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
                            <a class="navbar-brand" href="<?php echo $base_url; ?>"><img src="<?php echo $base_url; ?>asset/images/logo.jpg" alt="Education World"/></a> </div>
                        <!-- Collect the nav links, forms, and other content for toggling -->
                        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                            <ul class="nav navbar-nav">
                                <li><a href="<?php echo base_url(); ?>">Home</a></li>
                                <li><a href="<?php echo $base_url; ?>School/about_us">About Us</a></li>
                                <li class="dropdown"><a href="javascript:void(0)" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Message <i class="fa fa-angle-down"></i></a>
                                    <ul class="dropdown-menu">
                                        <li><a href="<?php echo $base_url; ?>School/chairman_m">Chairman Message</a></li>
                                        <li><a href="<?php echo $base_url; ?>School/principle_m">principle Message </a></li>
                                    </ul>
                                </li>

                                <li class="dropdown"> <a href="javascript:void(0)" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Staff <i class="fa fa-angle-down"></i></a>
                                    <ul class="dropdown-menu">
                                        <li><a href="<?php echo base_url(); ?>School/staff?id=1">Staff Development</a></li>
                                        <li><a href="<?php echo base_url(); ?>School/staff?id=2">Management Committee</a></li>
                                    </ul>
                                </li>

                                <li class="dropdown"> <a href="javascript:void(0)" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Admission <i class="fa fa-angle-down"></i></a>
                                    <ul class="dropdown-menu">
                                        <li><a href="#">Admission process</a></li>
                                        <li><a href="#">Admission & other Fees</a></li>
                                        <li><a href="<?php echo base_url() ?>School/faq">Frequently Asked Questions</a></li>
                                    </ul>
                                </li>
                                <li class="dropdown"> <a href="javascript:void(0)" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">News <i class="fa fa-angle-down"></i></a>
                                    <ul class="dropdown-menu">
                                        <li><a href="classic-news.html">All Notice</a></li>
                                        <li><a href="#">Recent News</a></li>
                                        <li><a href="#"> Carrer </a></li>
                                    </ul>
                                </li>
                                <li><a href="<?php echo base_url(); ?>school/gallery">Gallery</a></li>
                                <li><a href="<?php echo base_url(); ?>School/contact">Contact Us</a></li>
                            </ul>

                        </div>
                        <!-- /.navbar-collapse -->
                    </div>
                </nav>
            </div>
        </div>
    </header>

</html>

